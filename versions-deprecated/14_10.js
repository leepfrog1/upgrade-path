import flux from "@aust/react-flux";

flux.dispatch("version/add", {
  major: 14,
  minor: 10,
});
